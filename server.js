const path = require('path');
const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const formatMessage = require('./utils/messages');

const app = express();
const server = http.createServer(app);
const io = socketio(server);


//set static folder
app.use(express.static(path.join(__dirname, 'public')));

const botName = "Chat App Bot"

//Run when client connects
io.on('connection', socket => {
  socket.on('joinRoom', ({username, room}) => {
    

    socket.emit('message', formatMessage(botName, 'Welcome to ChatChord!'));
    //broadcast when a user connects
    socket.broadcast.emit('message', formatMessage(botName, "A user has joined the chat"));

  });

  //runs when client disconnects
  socket.on('disconnect', () => {
    io.emit('message', formatMessage(botName, 'A user has left the chat'));
  });

  //Listen for chatMessage
  socket.on('chatMessage', (msg) => {
    io.emit('message', formatMessage('USER', msg));
  });
});

const PORT = 3000;

server.listen(PORT, () => console.log(`Server running on port ${PORT}`));
